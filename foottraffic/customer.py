from random import random, choice, randint
from weightedtuple import *
from mallgeom import *

class Customer(object):
    """docstring for Customer"""
    def __init__(self, id, mall):
        super(Customer, self).__init__()
        self.id = id
        self.path = []
        self.destination = None
        self.mall = mall
        self.current_position = self.mall.getRandomEntrance().clone()
        self.probability_to_leave = float(randint(35,100))/float(100)
        # self.probability_to_leave = 1
        self.speed = 2
        self.is_leaving = False
        self.has_left = False
        self.likes = self.seedLikes()
        self.steps_towards_destination = 0


    def shouldGiveUp(self):
        if len(self.path) > 20 :
            if(self.path[-19].distance(self.current_position)<=7):
                return True
        return False
    
    def makeDest(self):
        if self.destination == None:
            self.destination = self.mall.getRandomStore()
        elif self.current_position.distance(self.destination.getCoordinate())<5:
            print "%s made it to %s!!!" %(self.id, self.destination)
            if self.is_leaving:
                self.has_left = True
            elif random() >= self.probability_to_leave:
                #choose a store
                cat = choice(self.likes)
                self.destination = choice(self.mall.getStoreByCategory(cat))
                # self.destination = self.mall.getRandomStore()
            else:
                self.destination = self.mall.getRandomEntrance()
                self.is_leaving = True
    
    def forceDestChange(self):
        print "%s has given up on making it to %s!!!" %(self.id, self.destination)
        self.steps_towards_destination  = 0
        if random() >= self.probability_to_leave and not self.is_leaving:
                #choose a store
            cat = choice(self.likes)
            self.destination = choice(self.mall.getStoreByCategory(cat))
                # self.destination = self.mall.getRandomStore()
        else:
            self.destination = self.mall.getRandomEntrance()
            self.is_leaving = True


    def seedLikes(self):
        cats = self.mall.getCategories()
        myLikes = {}
        prob_bank = 100
        for i in range(len(cats)):
            ammount_to_remove = randint(0, 100)
            if i == (len(cats) - 1):
                myLikes[cats[i]] = prob_bank
            else:
                myLikes[cats[i]] = ammount_to_remove
                prob_bank -= ammount_to_remove
        return WeightedTuple(myLikes)





    def step(self,time):
        if self.has_left:
            return
        else:
            self.steps_towards_destination += 1
            if self.shouldGiveUp():
                self.forceDestChange()
            dest = self.destination.getCoordinate()
            dx = self.speed
            dy = self.speed
            if dest.x > self.current_position.x:
                if dest.y > self.current_position.y:
                    dx = dx/2 + randint(-1, 2)
                    dy = dy/2 + randint(-1, 2)
                elif dest.y < self.current_position.y:
                    dy = -dy/2 + randint(-2, 1)
                    dx = dx/2 + randint(-1, 2)
                else:
                     dx = dx + randint(-1, 2)
                     dy = 0
            elif dest.x < self.current_position.x:
                if dest.y > self.current_position.y:
                    dx = -dx/2 + randint(-2, 1)
                    dy = dy/2 + randint(-1, 2)
                elif dest.y < self.current_position.y:
                    dy = -dy/2 + randint(-2, 1)
                    dx = -dx/2 + randint(-2, 1)
                else:
                    dy =0
                    dx = -dx + randint(-2, 1)
            else:
                if dest.y > self.current_position.y:
                    dx = 0
                    dy = dy + randint(-1, 2)
                elif dest.y < self.current_position.y:
                    dy = -dy + randint(-2, 1)
                    dx = 0
                else:
                    dy =0
                    dx = 0

            # if abs(dx) > abs(self.current_position.x - dest.x):
            #     dx = dest.x - self.current_position.x  
            # if abs(dy) > abs(self.current_position.y - dest.y):
            #     dy =  dest.y - self.current_position.y

            if dy < 0 and dest.y > self.current_position.y:
                print "%d %d %d" %(self.current_position.y, dy, dest.y)
               
            self.path.append(self.current_position.clone())
            dx,dy = self.doCollision(dx,dy)
            self.current_position.update(dx, dy)
            self.makeDest()

    def doCollision(self,dx,dy):
        new_coordinate = Coordinate(self.current_position.x + dx, self.current_position.y + dy)
        if new_coordinate.x<=0:
            dx = -dx
            print "ouch"
        if new_coordinate.x>=self.mall.maxx:
            dx = -dx
            print "ouch"
        if new_coordinate.y<=0 or new_coordinate.y >= self.mall.maxy:
            dy = -dy
            print "ouch"

        for rect in self.mall.forbidden:
            if rect.pointInside(new_coordinate):
                # print "Ouch %s is in %s" % (new_coordinate, rect)
                flag = False
                if not rect.xInside(self.current_position.x) and rect.xInside(new_coordinate.x):
                    flag = True
                    while True:
                        dx = randint(-2,2)
                        if not rect.xInside(Coordinate(self.current_position.x+ dx, self.current_position.y)):
                            break
                    
                if not rect.yInside(self.current_position.y) and rect.yInside(new_coordinate.y):
                    dy = 0
                    flag = True
                    while True:
                        dy = randint(-2,2)
                        if not rect.yInside(Coordinate(self.current_position.x, self.current_position.y+dy)):
                            break
                if not flag:
                    while True:
                        ndx = randint(-2,2)
                        ndy = randint(-2,2)
                        new_coordinate = Coordinate(self.current_position.x + ndx, self.current_position.y + ndy)
                        if not rect.pointInside(new_coordinate):
                            return ndx,ndy
        return dx,dy

        
    def __str__(self):
        return "Customer: " + str(self.id) + " is going to: " + \
        str(self.destination) + " is at: " + str(self.current_position)

        
    def __repr__(self):
        return str(self) 

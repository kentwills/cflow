from coordinate import *

class Store(object):
    """docstring for Store"""
    def __init__(self, name, x, y, id, cat):
        super(Store, self).__init__()
        self.name = name
        self.coordinate = Coordinate(x,y)
        self.id = id
        self.category = cat
        
    def getCategory(self):
        """docstring for getCategory"""
        return this.category

    def getCoordinate(self):
        return self.coordinate
        
    def __str__(self):
        return self.name 

    def __repr__(self):
        return str(self)
    

from coordinate import *


class Rectangle(object):
    """docstring for Rectangle"""
    def __init__(self, c1, c2):
        super(Rectangle, self).__init__()
        self.bl = c1
        self.tr = c2

    def pointInside(self,c):
        if c.y <= self.bl.y and c.y >= self.tr.y and c.x >= self.bl.x \
        and c.x <= self.tr.x:
            return True
        return False
    
    def xInside(self, x):
        if x >= self.bl.x and x <= self.tr.x:
            return True
        return False

    def yInside(self, y):
        if y <= self.bl.y and y >= self.tr.y:
            return True
        return False

    def __str__(self):
        return "(%s,%s)" % (self.bl,self.tr)

class Polygon(object):
    """docstring for Polygon"""
    def __init__(self, LoP):
        super(Polygon, self).__init__()
        self.LoP = LoP
        

        bigx =0
        smallx = 300000
        bigy = 0
        smally = 300000

        for p in self.LoP:
            if p.x > bigx:
                bigx=p.x
            elif p.x < smallx:
                smallx = p.x

            if p.y > bigy:
                bigy = p.y
            elif p.y < smally:
                smally = p.y

        self.bounder = Rectangle(Coordinate(smallx,bigy), Coordinate(bigx,smally))



    def __getitem__(self,index):
        return self.LoP[index]
        
    def pointInside(self,c):
        # determine if a point is inside a given polygon or not
    # Polygon is a list of (x,y) pairs.
        n = len(self.LoP)
        poly = self.LoP
        inside =False
        p1x,p1y = poly[0]
        for i in range(n+1):
            p2x,p2y = poly[i % n]
            if c.y > min(p1y,p2y):
                if c.y <= max(p1y,p2y):
                    if c.x <= max(p1x,p2x):
                        if p1y != p2y:
                            xinters = (c.y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                        if p1x == p2x or c.x <= xinters:
                            inside = not inside
            p1x,p1y = p2x,p2y

        return inside

    def xInside(self, x):
        return self.bounder.xInside(x)

    def yInside(self, y):
        return self.bounder.yInside(y)


def main():
    testCoord = Coordinate(230,340)
    testPolygon = Polygon([Coordinate(225,350),Coordinate(210,310),Coordinate(290,310),Coordinate(290,350)])
    testCoord2 = Coordinate(230,400)
    print testPolygon.pointInside(testCoord)
    print testPolygon.pointInside(testCoord2)

if __name__ == '__main__':
    main()
from store import *
from random import choice
from mallgeom import *

class Mall(object):
    """docstring for Mall"""
    def __init__(self, name):
        super(Mall, self).__init__()
        self.name = name
        self.stores = []
        self.entrances = []
        self.forbidden = []
        self.categories = None
        self.maxx = 1870
        self.maxy = 727

    def addForbidden(self, shape):
        self.forbidden.append(shape)

    def addStore(self, store):
        self.stores = self.stores + [store]

    def addEntrance(self, x, y):
        self.entrances.append(Coordinate(x, y))
        
    def getStoreByCategory(self, category):
        to_return = []
        for store in self.stores:
            if store.category == category:
                to_return = to_return + [store]
        return to_return

    def getCategories(self):
        if self.categories == None:
            self.categories = []
            for store in self.stores:
                if store.category not in self.categories:
                    self.categories.append(store.category)
            return self.categories
        else:
            return self.categories
        
    def getRandomStore(self):
        return choice(self.stores)

    def getRandomEntrance(self):
        return choice(self.entrances)
                
from customer import *
from store import *
from coordinate import *
from mall import *
from mallgeom import *
from random import random

class PathGenerator(object):
    """docstring for pathGenerator"""
    def __init__(self):
        super(PathGenerator, self).__init__()
        self.mall = Mall("Our Mall")
        self.customers = []
        self.customer_counter = 0
        self.customer_acquisition_rate = .005
        

    def addDefaults(self):
        self.mall.addStore(Store('Macys',307,409,1,'clothing'))
        self.mall.addStore(Store('Nike',451,185,2,'clothing'))
        self.mall.addStore(Store('TMobile',471,460,3,'phone'))
        self.mall.addStore(Store('Verizon',887,440,4,'phone'))
        self.mall.addStore(Store('ATT',1260,462,5,'phone'))
        self.mall.addStore(Store('Safeway',1561,385,6,'grocery'))
        self.mall.addStore(Store('Adidas',1373,199,7,'clothing'))
        self.mall.addStore(Store('BoA',1071,135,8,'bank'))
        self.mall.addStore(Store('CapitalOne',801,135, 9, 'bank'))

        self.mall.addEntrance(325, 319)
        self.mall.addEntrance(1531, 315)

        # self.mall.addForbidden(Rectangle(Coordinate(324, 232), 
        #     Coordinate(356, 0)))
        # self.mall.addForbidden(Rectangle(Coordinate(496, 130), 
        #     Coordinate(1321 ,0)))
        # self.mall.addForbidden(Rectangle(Coordinate(1321, 130), 
        #     Coordinate(1601, 0)))
        # self.mall.addForbidden(Rectangle(Coordinate(41, 723), 
        #     Coordinate(1828, 472)))

        self.mall.addForbidden(Polygon([Coordinate(323,263),Coordinate(269,97),
            Coordinate(490,23),Coordinate(490,131),Coordinate(355,263)]))

        self.mall.addForbidden(Polygon([Coordinate(494,131),Coordinate(494,30),
            Coordinate(550,0),Coordinate(661,0),Coordinate(661,134)]))

        self.mall.addForbidden(Polygon([Coordinate(494,131),Coordinate(494,30),
            Coordinate(550,0),Coordinate(661,0),Coordinate(661,134)]))

        self.mall.addForbidden(Rectangle(Coordinate(661,134),Coordinate(1199,0)))

        self.mall.addForbidden(Polygon([Coordinate(1200,0),Coordinate(1593,101),Coordinate(1532,260),
            Coordinate(1450,263),Coordinate(1313,140),Coordinate(1204,133)]))

        self.mall.addForbidden(Polygon([Coordinate(2,379),Coordinate(257,295),Coordinate(323,479),
            Coordinate(535,475),Coordinate(535,729),Coordinate(2,729)]))

        self.mall.addForbidden(Polygon([Coordinate(541,475),Coordinate(669,391),Coordinate(707,387),
            Coordinate(707,729),Coordinate(541,729)]))

        self.mall.addForbidden(Rectangle(Coordinate(725,703),Coordinate(1079,451)))
        self.mall.addForbidden(Rectangle(Coordinate(1079,703),Coordinate(1455,467)))

        self.mall.addForbidden(Polygon([Coordinate(1479,643),Coordinate(1475,467),Coordinate(1533,465),
            Coordinate(1561,409),Coordinate(1619,227),Coordinate(1877,313),Coordinate(1735,727)]))

        self.mall.addForbidden(Rectangle(Coordinate(483,325), Coordinate(873,243)))

        self.mall.addForbidden(Rectangle(Coordinate(963,325),Coordinate(1281,243)))








    def addCustomers(self,num):
        for i in range(num):
            self.customers.append(Customer(self.customer_counter, self.mall))
            self.customers[-1].makeDest()
            self.customer_counter+=1

    def customersPresent(self):
        for customer in self.customers:
            if not customer.has_left:
                print customer
                return True
            return False


    def moreCustomerCheck(self):
        probs = random()
        if self.customer_acquisition_rate > probs:
            self.customers.append(Customer(self.customer_counter, self.mall))
            self.customers[-1].makeDest()
            self.customer_counter+=1

    def outputPaths(self):
        fp = open("paths.txt",'w')
        for customer in self.customers:
            fp.write("%s;" %(customer.path))


def main():
    ourGen = PathGenerator()
    ourGen.addDefaults()
    # ourGen.addCustomers(1)
    # for time in range(39600):
    time = 0
    while time < 2000:
        ourGen.moreCustomerCheck()
        for customer in ourGen.customers:
            customer.step(time)
        time += 1

    while ourGen.customersPresent():
        for customer in ourGen.customers:
            customer.step(time)
        time += 1



    ourGen.outputPaths()



if __name__ == '__main__':
    main()
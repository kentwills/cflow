require('./moment.min.js');

var stores = [
{ 
	id: 1, name: 'Macys', x: 318, y: 618
},
{ 
	id: 2, name: 'Nike', x: 520, y: 278
},
{ 
	id: 3, name: 'TMobile', x: 511, y: 633
},
{ 
	id: 4, name: 'Verizon', x: 921, y: 604
},
{ 
	id: 5, name: 'ATT', x: 1260, y: 629
},
{ 
	id: 6, name: 'Safeway', x: 1586, y: 543
},
{ 
	id: 7, name: 'Adidas', x: 1371, y: 320
},
{ 
	id: 8, name: 'BoA', x: 1076, y: 272
},
{ 
	id: 9, name: 'CapitalOne', x: 789, y: 272
},
];

var probOfBeingInStore = 0.80;

var people = [];
var paths = [];
var pathCounter = 0;
var customers = [];
var profits = [];

generateInitialState(6, '10:00', 'HH:mm');
console.log(paths);

function generateInitialState(numPeople, startTime, timeFormat) {
	var notStores = Math.round(((1 - probOfBeingInStore)*stores.length)/probOfBeingInStore);
	for(var i = 1; i <= numPeople; i++) {
		people.push({id: i});
		//var whereInMall = stores[Math.floor(Math.random() * stores.length+notStores)];
		var whereInMall = stores[Math.floor(Math.random() * stores.length)];
		paths.push({
			id:++pathCounter, 
			time: moment(startTime, timeFormat).unix()*1000,
			person: i,
			x: whereInMall.x,
			y: whereInMall.y,
			store: whereInMall.id
		});
	}
}
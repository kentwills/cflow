import math

class Coordinate(object):
    """docstring for Coordinate"""

    def __init__(self, x,y):
        super(Coordinate, self).__init__()
        self.x = x
        self.y = y

    def __str__(self):
        return "%d, %d" %(self.x, self.y)
    
    def __repr__(self):
        return str(self)

    def __iter__(self):
        for num in [self.x,self.y]:
            yield num

    def getCoordinate(self):
        return self

    def distance(self, other_coordinate):
        return math.sqrt(((self.x-other_coordinate.x)**2) + \
            ((self.y-other_coordinate.y)**2))
    
    def clone(self):
        return Coordinate(self.x, self.y)

    def update(self, dx, dy):
        self.x += dx
        self.y += dy


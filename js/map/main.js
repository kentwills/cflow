var assetManager = new AssetManager();

assetManager.queueDownload('img/loading.gif');

assetManager.downloadAll(function() {
	Map.init();
	
    $('#map').bind('mousewheel', onMouseWheel);
});

function onMouseWheel(e, delta) {
    console.log('>>>>',e,delta);
    delta = e.originalEvent.wheelDelta;
    //prevent only the actual wheel movement
    if (delta !== 0) {
        e.preventDefault();
    }
    var min_scale = 0.1;
    var scale=1;

    var cur_scale;
    console.log(cur_scale);
    if (delta > 0) {
        cur_scale = scale + Math.abs(delta / 640);
    } else {
        cur_scale = scale - Math.abs(delta / 640);
    }

    //check for minimum scale limit
    /*
    if (cur_scale > min_scale) {

        var d = document.getElementById('map');
        var cnvsPos = getPos(d);

        var Apos = Map.stage.getAbsolutePosition();

        var mousePos = Map.stage.getMousePosition();

        var smallCalc  = (e.pageX - Apos.x - cnvsPos.x)/scale;
        var smallCalcY = (e.pageY - Apos.y - cnvsPos.y)/scale;

        var endCalc = (e.pageX - cnvsPos.x) - cur_scale*smallCalc;
        var endCalcY = (e.pageY - cnvsPos.y) - cur_scale*smallCalcY;

        scale = cur_scale;
        console.log(cur_scale, Map.getScale());

        Map.stage.setPosition( endCalc, endCalcY);

        Map.stage.setScale(cur_scale);
        Map.stage.draw();
    }
    */
}

function getPos(el){
    for (var lx=0, ly=0;
         el != null;
         lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
    return {x: lx,y: ly};
}
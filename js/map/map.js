/**
 * MallMap Script
 * Ruofei Du
 */
var Map = {
    /**
     * Settings Object, holding application wide settings
     */
    Settings :{
        globalScale: 0.7,
        numStores:   0,
        initX:  0.0,    initY:  0.0,
        absX:   10.0,   absY:   24.0,
        maxWidth:       1500,   maxHeight: 900,
        canvasWidth:    0.0,    canvasHeight: 0.0,
        zoom:   1,
        inflow: 1, outflow: 1, flowall: 1, zoomin:1, zoomout: 0,
        minScale: 1, microZoomIn: 0.04, stepZoomIn: 0.5,
        maxScale: 5, microZoomOut: 0.04, stepZoomOut: 0.5,
        initialized: false,
        colors: ["#d6b963", "#dadada", "#8AA197", "#4b6b6e"],
        server: 'http://nameless-bastion-3282.herokuapp.com/getData?'
    },

    Data :{
        storeAjax: Array(),
        storeData: Array(),
        storeColorID: Array(),
        absAjax: Array(),
        absData: Array()
    },

    State :{
        hour1: 9,
        hour2: 22,
        day1: 1,
        day2: 7,
        month1: 1,
        month2: 12,
        value: 'customers',
        timeType: 'hour',
    },

    margin: { top: 50, right: 10, bottom: 50, left: 150 },

    /**
     * Our main stores object and feet object
     */
    stores: {},
    feet: {},
    absData: {},
    txt: {},
    txtRect: {},
    entrances: {}, 
    flowLegend: null, flowLegend2: null,
    absNum: 0,
    toZoom: 0, 
    zoomAnimition: null,

    stage: null,
    mapLayer: null,
    topLayer:  null,
    fullLegendLayer: null,
    rightLegendLayer: null,
    footLayer: null,
    textLayer: null,
    absLayer: null,

    updateTime:function (timeStart, timeEnd, dayStart, dayEnd, monthStart, monthEnd) {
        Map.State.hour1 = timeStart;
        Map.State.hour2 = timeEnd; 
        Map.State.day1 = dayStart; 
        Map.State.day2 = dayEnd;
        Map.State.month1 = monthStart;
        Map.State.month2 = monthEnd; 
        Map.queryStoreData(); 
    },

    updateType:function (type) {
        Map.State.value = type; 
        Map.queryStoreData(); 
    },

    reloadMap: function() {
        Map.Settings.canvasWidth = document.getElementById('map').offsetWidth;
        Map.Settings.canvasHeight = document.getElementById('map').offsetHeight;
        Map.Settings.globalScale = Math.min(Map.Settings.canvasWidth / Map.Settings.maxWidth, Map.Settings.canvasHeight / Map.Settings.maxHeight);
        Map.updateZoomLevel(0, 0); 
    },

    init: function() {
        //Initiate a Kinetic stage
        Map.Settings.canvasWidth = document.getElementById('map').offsetWidth;
        Map.Settings.canvasHeight = document.getElementById('map').offsetHeight;
        Map.Settings.globalScale = Math.min(Map.Settings.canvasWidth / Map.Settings.maxWidth, Map.Settings.canvasHeight / Map.Settings.maxHeight);

        Map.stage = new Kinetic.Stage({
            container: 'map',
            width: Map.Settings.canvasWidth,
            height: Map.Settings.canvasHeight,
            draggable: true,
            scale: Map.Settings.globalScale,
            
            dragBoundFunc: function(pos) {
                newX = pos.x; newY = pos.y; 
                Map.buttonLayer.setX(-newX/(Map.Settings.globalScale * Map.Settings.zoom)); 
                Map.buttonLayer.setY(-newY/(Map.Settings.globalScale * Map.Settings.zoom)); 
                return { x: newX, y: newY };
            }
            
        });

        Map.mapLayer = new Kinetic.Layer({});       // for the map
        Map.topLayer = new Kinetic.Layer({});       // for the map hovering
        Map.buttonLayer = new Kinetic.Layer({ });   // for the zoom in and zoom out button
        Map.footLayer = new Kinetic.Layer({});      // for the original raw foot traffic data
        Map.textLayer = new Kinetic.Layer();        // for the text above the map
        Map.absLayer = new Kinetic.Layer();         // for the new abstarct foot traffic data
        Map.fullLegendLayer = new Kinetic.Layer();         // for the new abstarct foot traffic data
        Map.rightLegendLayer = new Kinetic.Layer();         // for the new abstarct foot traffic data
        
        Map.drawButtonLayer(); 

        Map.queryStoreData(); 

        Map.stage.add(Map.mapLayer);
        Map.stage.add(Map.topLayer);
        Map.stage.add(Map.footLayer);
        Map.stage.add(Map.textLayer); 
        Map.stage.add(Map.absLayer);
        Map.stage.add(Map.buttonLayer);
        Map.stage.add(Map.fullLegendLayer);
        Map.stage.add(Map.rightLegendLayer);
        
        Map.mapLayer.draw();
    },

    dist: function(p1, p2) {
        return Math.sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y));
    },

    lineCut: function(p1, p2, p1ratio) {
        var res = {x: p1.x*p1ratio+p2.x*(1-p1ratio), y: p1.y*p1ratio+p2.y*(1-p1ratio)}; 
        return res; 
    },

    // pos, width: a double in [0,1]
    getStoreFlow: function(storeIn, storeOut, inPos, inWidth, outPos, outWidth, arrow) {
        // define the 4 key points
        var leftUpper = {x: Map.stores[storeIn].lx, y: Map.stores[storeIn].ly}; 
        var leftLower = {x: Map.stores[storeIn].rx, y: Map.stores[storeIn].ry}; 
        var rightUpper = {x: Map.stores[storeOut].lx, y: Map.stores[storeOut].ly}; 
        var rightLower = {x: Map.stores[storeOut].rx, y: Map.stores[storeOut].ry}; 
        var avgWidth = (Map.dist(leftUpper, leftLower) + Map.dist(rightUpper, rightLower)) / 4; 
        var rightReserved = false, leftReserved = false; 

        // the control points are parts of the store entrance
        if (inPos != 0 || inWidth != 0 || outPos != 0 || outWidth != 0) {
            var lu = leftUpper, ll = leftLower, ru = rightUpper, rl = rightLower; 
            leftUpper = Map.lineCut(lu, ll, inPos + inWidth); 
            leftLower = Map.lineCut(ll, lu, 1-inPos + inWidth); 
            rightUpper = Map.lineCut(ru, rl, outPos + outWidth); 
            rightLower = Map.lineCut(rl, ru, 1-outPos + outWidth); 
        }

        if (storeIn == storeOut) {
            var t = rightUpper; rightUpper = rightLower; rightLower = t; 
        }
        // define the centural points
        var centerLeft = {x: (leftUpper.x + rightUpper.x) / 2, y: (leftUpper.y + rightUpper.y) / 2}; 
        var centerRight = {x: (leftLower.x + rightLower.x) / 2, y: (leftLower.y + rightLower.y) / 2}; 

        //a special list
        if (storeIn == 9 && storeOut == 8){ var t = rightUpper; rightUpper = rightLower; rightLower = t; rightReserved = true; }
        if (storeIn == 3 && storeOut == 4) { var t = leftUpper; leftUpper = leftLower; leftLower = t; leftReserved = true; centerLeft = {x:633, y:240}; centerRight = {x:633, y:240}; }
        if (storeIn == 5 && storeOut == 7) { var t = leftUpper; leftUpper = leftLower; leftLower = t; leftReserved = true; }
        if (storeIn == 5 && storeOut == 6) { var t = rightUpper; rightUpper = rightLower; rightLower = t; rightReserved = true; }
        if (storeIn == 6 && storeOut == 5) { var t = rightUpper; rightUpper = rightLower; rightLower = t; rightReserved = true; }
        if (storeIn == 3 && storeOut == 2) { var t = leftUpper; leftUpper = leftLower; leftLower = t; var t = rightUpper; rightUpper = rightLower; rightLower = t; rightReserved = true; }
        if (storeIn == 7 && storeOut == 6) { var t = leftUpper; leftUpper = leftLower; leftLower = t; }
        if (storeIn == 5 && storeOut == 5) { centerLeft = {x:1440, y:409}; centerRight = {x:1440, y:409-avgWidth}; }
        if (storeIn == 1 && storeOut == 1) { centerLeft = {x:1548, y:333}; centerRight = {x:1530, y:316};}
        if (storeIn == 9 && storeOut == 9) { centerLeft = {x:339, y:382}; centerRight = {x:335, y:378}; }
        if (storeIn == 4 && storeOut == 4) { centerLeft = {x:588, y:248}; centerRight = {x:580, y:240}; }

        // for parallel occasions
        if (Math.abs(leftUpper.y-rightUpper.y) + Math.abs(leftLower.y-rightLower.y) < 10) {
            if (storeIn != storeOut) 
            if (Map.stores[storeIn].rx > Map.stores[storeIn].rx) {
                var temp = rightUpper.x; 
                rightUpper.x = rightLower.x; 
                rightLower.x = temp; 
            }
            else {
                var temp = leftUpper.x; 
                leftUpper.x = leftLower.x; 
                leftLower.x = temp; 
            }
        }
        
        // calculate the centural points
        var centerX = (centerRight.x + centerLeft.x) / 2;
        var cp1Upper = {x: centerX, y: leftUpper.y};
        var cp2Upper = {x: centerX, y: rightUpper.y};
        var cp1Lower = {x: centerX, y: rightLower.y};
        var cp2Lower = {x: centerX, y: leftLower.y};

        if (Math.abs(leftUpper.y-rightUpper.y) + Math.abs(leftLower.y-rightLower.y) < 10) {
            centerLeft = {x: (leftUpper.x + rightUpper.x) / 2, y: 300 }; 
            centerRight = {x: (leftLower.x + rightLower.x) / 2, y: 372 }; 

            var cp1Upper = {x: centerX, y: 370+inWidth*100};
            var cp2Upper = {x: centerX, y: 370+inWidth*100};
            var cp1Lower = {x: centerX, y: 370};
            var cp2Lower = {x: centerX, y: 370};
        }

        if ((storeIn == 7 || storeIn == 6) && storeOut == 8) {
            var t = cp1Upper; cp1Upper = cp1Lower; cp1Lower = t; 
            var t = cp2Upper; cp2Upper = cp2Lower; cp2Lower = t; 
        }

        if (storeIn == 7 && storeOut == 6) {
            var t = leftUpper; leftUpper = leftLower; leftLower = t; 
            centerLeft = {x: (leftUpper.x + rightUpper.x) / 2, y: 300 }; 
            centerRight = {x: (leftLower.x + rightLower.x) / 2, y: 372 }; 

            var cp1Upper = {x: centerX, y: 460+inWidth*100};
            var cp2Upper = {x: centerX, y: 460+inWidth*100};
            var cp1Lower = {x: centerX, y: 460};
            var cp2Lower = {x: centerX, y: 460};
        }

        var ml = {x:0, y:0}, mu = {x:0, y:0}, mml = {x:0, y:0}, mmu = {x:0, y:0} ;
        // the main flow curve
        if (arrow) {
            var t = 10;
            var a = rightLower.x; 
            var b = rightLower.y;
            var c = rightUpper.x; 
            var d = rightUpper.y; 

            var x = a - c; 
            var y = b - d; 
            var z = Math.sqrt(x*x+y*y); 
            var sint = y / z;
            var cost = x / z;

            if (storeOut <= 4 && storeOut >= 1 || (storeIn == 5 && storeOut == 6)) { 
                ml = {x: c + x - t * sint, y: d + y + t * cost};
                mu = {x: c - t * sint, y: d + t * cost}; 
            } else {
                ml = {x: c + x + t * sint, y: d + y - t * cost};
                mu = {x: c + t * sint, y: d - t * cost}; 
            }

            mml.x = Map.lineCut(ml, mu, 0.9); 
            mmu.y = Map.lineCut(ml, mu, 0.1); 

            centerLeft = {x: (leftUpper.x + mu.x) / 2, y: (leftUpper.y + mu.y) / 2}; 
            centerRight = {x: (leftLower.x + ml.x) / 2, y: (leftLower.y + ml.y) / 2}; 

            centerX = (centerRight.x + centerLeft.x) / 2;
            cp1Upper = {x: centerX, y: leftUpper.y};
            cp2Upper = {x: centerX, y: mu.y};
            cp1Lower = {x: centerX, y: ml.y};
            cp2Lower = {x: centerX, y: leftLower.y};
        }

        //console.log(mu); 
        // recalculate
        //avgWidth = (Map.dist(leftUpper, leftLower) + Map.dist(rightUpper, rightLower)) / 4; 
        
        
        var flow;

        if (!arrow){
            flow = new Kinetic.Shape({
                drawFunc: function(canvas) {
                    var context = canvas.getContext('map');
                    context.fillStyle = this.getFill();
                    context.beginPath();
                    context.moveTo(leftUpper.x, leftUpper.y);
                    context.bezierCurveTo(cp1Upper.x, cp1Upper.y, cp2Upper.x, cp2Upper.y, rightUpper.x, rightUpper.y);
                    context.lineTo(rightLower.x, rightLower.y);
                    context.bezierCurveTo(cp1Lower.x, cp1Lower.y, cp2Lower.x, cp2Lower.y, leftLower.x, leftLower.y);
                    context.lineTo(leftUpper.x, leftUpper.y);
                    context.fill();
                    canvas.stroke(this);
                },
                x: Map.Settings.absX, 
                y: Map.Settings.absY,
                opacity: 0.5,
                fill: '#aaa',
                //stroke: 'red',
                strokeWidth: 0.5
            }); 
            /*
            flow.on('mouseover', function() {
                console.log("flow mouseover"); 
            });
            */

        } else {

            flow = new Kinetic.Shape({
                drawFunc: function(canvas) {
                    var context = canvas.getContext('map');
                    context.fillStyle = this.getFill();
                    context.beginPath();
                    context.moveTo(leftUpper.x, leftUpper.y);
                    context.bezierCurveTo(cp1Upper.x, cp1Upper.y, cp2Upper.x, cp2Upper.y, mmu.x, mmu.y);

                    context.lineTo(mu.x, mu.y);
                    context.lineTo((rightUpper.x+rightLower.x)/2, (rightUpper.y+rightLower.y)/2);
                    context.lineTo(mml.x, mml.y);
                    context.lineTo(ml.x, ml.y);
                    context.bezierCurveTo(cp1Lower.x, cp1Lower.y, cp2Lower.x, cp2Lower.y, leftLower.x, leftLower.y);
                    context.lineTo(leftUpper.x, leftUpper.y);
                    context.fill();
                    canvas.stroke(this);
                },
                x: Map.Settings.absX, 
                y: Map.Settings.absY,
                opacity: 0.5,
                fill: '#aaa',
                //stroke: 'red',
                strokeWidth: 0.5
            }); 

            
        }
        return flow; 
    },

    addAbslayer: function(storeIn, storeOut, inPos, inWidth, outPos, outWidth, arrow) {
        Map.absData[Map.absNum] = Map.getStoreFlow(storeIn, storeOut, inPos, inWidth, outPos, outWidth, 0); 

        Map.absData[Map.absNum].on('click', function() {
            console.log("click" + Map.absNum); 
        });

        Map.absLayer.add(Map.absData[Map.absNum]);
        (function(i) {
            Map.absData[i].on('mouseout', function() {
                Map.absData[i].setOpacity(0.5); 
                console.log("mouseout" + i); 
            });

            Map.absData[i].on('mouseenter', function() {
                Map.absData[i].setOpacity(1.0); 
                console.log("mouseenter" + i); 
            });

            Map.absData[i].on('mouseover', function() {
                Map.absData[i].setOpacity(1.0); 
                console.log("mouseover" + i); 
            });
        })(Map.absNum);


        Map.absNum = Map.absNum + 1; 
    },

    constructPathQuery: function(){
        var s = Map.Settings.server
            +'type='+"paths"
            +'&timeType='+Map.State.timeType 
            +'&day1='+Map.State.day1
            +'&day2='+Map.State.day2
            +'&month1='+Map.State.month1
            +'&month2='+Map.State.month2
            +'&hour1='+Map.State.hour1
            +'&hour2='+Map.State.hour2;
            //console.log(s);
        return s;
    },

    constructStoresQuery: function(name){
        
        var s = Map.Settings.server
            +'type='+"stores"
            +'&name='+name
            +'&value='+Map.State.value
            +'&timeType='+"all"
            +'&hour1='+Map.State.hour1
            +'&hour2='+Map.State.hour2
            +'&day1='+Map.State.day1
            +'&day2='+Map.State.day2
            +'&month1='+Map.State.month1
            +'&month2='+Map.State.month2;
        
        //var s='http://nameless-bastion-3282.herokuapp.com/getData?type=stores&name=all&value=customers&timeType=month&hour1=12&hour2=20&day1=4&day2=7&month1=4&month2=4';
        return s;
    },

    queryAbsData: function(){
        var dataRequest = $.getJSON(
            Map.constructPathQuery(), function(response){
            Map.Data.absAjax = response.paths;
            //$("#overlay").fadeOut('slow');
            var n = Map.Data.absAjax.length;
            //console.log(n, response.paths); 
            for (var i = 0; i < n; ++i) {
                Map.Data.absData[i] = Array();
                Map.Data.absData[i][0] = parseInt(response.paths[i].start);
                Map.Data.absData[i][1] = parseInt(response.paths[i].end);
                Map.Data.absData[i][2] = response.paths[i].value;
            }
            Map.drawAbsLayer(-1); 
        }).error(function() { alert("error"); });
        return dataRequest;
    },

    queryStoreData: function(){
        var dataRequest = $.getJSON(
            Map.constructStoresQuery("all"), function(response){

            if (!Map.Settings.initialized) {
                var n = response.rows.length; 
                for (var i = 0; i < n; ++i) {
                    storeNames[i+1] = response.rows[i].store; 
                    //console.log(i+1, response.rows[i]); 
                }
            }

            Map.Data.storeAjax = response.values;
            for (var i = 0; i < Map.Data.storeAjax.length; ++i) {
                Map.Data.storeData[Map.Data.storeAjax[i].store] = Map.Data.storeAjax[i].value;
            }
            Map.setupMapPathData();
        }).error(function() { alert("load store data error"); });
        return dataRequest;
    },

    setupMapPathData: function() {
        //console.log("Setup Map Path Data");
        Map.Settings.numStores = Map.Data.storeAjax.length;
        //console.log("Total Stores: " + Map.Settings.numStores); 
        var count = 0;
        var values = [];
        
        var min = 1e6, max = 0; 
        for (var id in Map.Data.storeData) {
            var w = Map.Data.storeData[id]; 
            if (w > max) max = w; 
            if (w < min) min = w; 
            values.push(); 
        }
        var piece = (max-min) / Map.Settings.colors.length; 
        Map.Data.storeColorID = Array(); 

        for (var id in Map.Data.storeData) {
            var w = Map.Data.storeData[id]; 
            var colorID = Math.floor((w - min) / piece); 
            if (colorID == Map.Settings.colors.length) colorID = Map.Settings.colors.length - 1; 
            Map.Data.storeColorID[id] = colorID; 
        }
        
        //console.log(Map.Data.storeColorID); 

        for(id in storeNames) {
            var pathObject = new Kinetic.Path({
                data: mapPathData[id].path,
                x: Map.Settings.initX,
                y: Map.Settings.initY,
                //stroke: '#fff',
                //strokeWidth: 6,
                id: id 
            });
            //console.log(Map.Data.storeData[id]); 

            Map.stores[id] = {
                name: storeNames[id],
                lx: mapCordData[id].lx,
                ly: mapCordData[id].ly,
                rx: mapCordData[id].rx,
                ry: mapCordData[id].ry,
                path: pathObject,
                id: id,
                color: null,
            };

            Map.stores[id].color = Map.Data.storeColorID[id]; 
            Map.stores[id].path.setFill(Map.Settings.colors[Map.Data.storeColorID[id]]);
            //console.log(Map.Settings.colors[Map.Data.colorAjax[count]]);
            ++count;
            Map.stores[id].path.setOpacity(1.0);

            if (count == Map.Settings.numStores) {
                //finally
                if (Map.Settings.initialized == false) {
                    Map.Settings.initialized = true; 
                    Map.drawTextLayer(); 
                }
                Map.drawstores(); 
                Map.queryAbsData(); 
            }

            function getRandomColor() {
                var randomNum = Math.floor(Math.random()*4);
                return randomNum;
            }
        }


    },

    drawAbsLayer: function(specifiedStore){
        Map.absLayer.removeChildren(); 
        Map.absNum = 0; 
        var sum = 0; 
        var n = Map.Data.absAjax.length;
        Map.absLayer.clear();
        var acc = Array(); 
        var data = Array(); 
        // copy data
        for (var i = 0; i < n; ++i) {
            data[i] = Array(); 
            for (var j = 0; j < 3; ++j) data[i][j] = Map.Data.absData[i][j]; 
        }

        // sum data for normalization
        for (var i = 0; i < n; ++i) if (data[i][0] != 0 && data[i][1] != 0) {
            acc[i] = true; 
            if (data[i][0] == specifiedStore || specifiedStore == -1){
                sum = sum + data[i][2];  
            }
        } else acc[i] = false; 

        var pos = Array(), tot = Array(); 
        for (i in storeNames) {
            pos[i] = 0; 
            tot[i] = 0; 
        }

        // calculate the pos
        if (specifiedStore == -1) {
        // overall rendering    
            var vd = Array(); 
            var maxTot = 0; 
            for (var i = 0; i < n; ++i) if (acc[i]) {
                vd[i] = false; 
                var x = data[i][0];
                var y = data[i][1]; 
                var w = data[i][2];         
                tot[x] = tot[x] + w / sum; 
                if (x != y) tot[y] = tot[y] + w/sum; 
                maxTot = Math.max(maxTot, tot[x]); 
                maxTot = Math.max(maxTot, tot[y]);    
            }
            for (i in storeNames) {
                pos[i] = (1 - tot[i] / maxTot) / 2;
            }

            // aggregate the outcome flow to the income flow
            for (var i = 0; i < n; ++i) if (!vd[i]) if (acc[i]) {
                for (var j = i+1; j < n; ++j) if (!vd[j]) if (acc[j]) {
                    if (data[i][0] == data[j][1] && data[i][1] == data[j][0]) {
                        vd[i] = true; 
                        data[j][2] = data[i][2] + data[j][2]; 
                    }
                }
            }

            //
            var m = Map.Settings.numStores;
            for (var i = 1; i <= m; ++i) {
                list = Array();
                for (var j = 0; j < n; ++j) if (data[j][0] == i && !vd[j] && acc[j]) {
                    var struct = {id : j, angel : Map.getStoreAngel(i, data[j][1])};
                    list.push(struct); 
                }

                list.sort(sortNumber); 

                //console.log(i, list); 
                for (var k = 0; k < list.length; ++k) {
                    var j = list[k].id;
                    var x = data[j][0];
                    var y = data[j][1]; 
                    if (x == y) continue; 
                    var w = data[j][2] / sum / maxTot; 
                    Map.addAbslayer(x, y, pos[x] + w/2, w/2, pos[y]+w/2, w/2, 0);
                    pos[x] = pos[x] + w;
                    pos[y] = pos[y] + w;    
                }
            }
            
            // find the pairs of flows
            /*
            for (var i = 0; i < n; ++i) if (!vd[i]) if (acc[i]){
                var x = data[i][0];
                var y = data[i][1]; 
                if (x == y) continue; 
                var w = data[i][2] / sum / maxTot; 
                Map.addAbslayer(x, y, pos[x] + w/2, w/2, pos[y]+w/2, w/2 );
                pos[x] = pos[x] + w;
                pos[y] = pos[y] + w; 
            }
            */

        } else {
        // partial rendering
            function sortNumber(a, b) {
                return a.angel - b.angel;
            }
        // sort
            list = Array();

            for (var i = 0; i < n; ++i) if (acc[i]) if (Map.Data.absData[i][0] == specifiedStore) {
                var struct = {id : i, angel : Map.getStoreAngel(specifiedStore, Map.Data.absData[i][1])};
                //console.log(specifiedStore, Map.Data.absData[i][1], Map.getStoreAngel(specifiedStore, Map.Data.absData[i][1])); 
                list.push(struct); 
            }
            list.sort(sortNumber); 
            //console.log(list); 


            for (var k = 0; k < list.length; ++k) {

                var i = list[k].id;
                var x = Map.Data.absData[i][0];
                var y = Map.Data.absData[i][1]; 
                var w = Map.Data.absData[i][2] / sum; 

                if (x == y){
                    Map.addAbslayer(x, y, pos[x] + w/2, w/2, pos[x] + w/2, w/2, 1); 
                } else {
                    Map.addAbslayer(x, y, pos[x] + w/2, w/2, 0.5, w/2, 1); 
                }
                pos[x] = pos[x] + w; 
            }

            /*
            for (var i = 0; i < n; ++i) if (acc[i]) if (Map.Data.absData[i][0] == specifiedStore) {
                var x = Map.Data.absData[i][0];
                var y = Map.Data.absData[i][1]; 
                var w = Map.Data.absData[i][2] / sum; 
                
                if (x == y){
                    Map.addAbslayer(x, y, pos[x] + w/2, w/2, pos[x] + w/2, w/2 ); 
                } else {
                    Map.addAbslayer(x, y, pos[x] + w/2, w/2, 0.5, w/2 ); 
                }
                pos[x] = pos[x] + w; 
            }
            */

        }
        
        //for (var i = 0; i < Map.absNum; ++i) Map.absLayer.add(Map.absData[i]);
        Map.absLayer.draw(); 
    },

    getSqr: function(x, y) {
        return Math.sqrt(x*x+y*y); 
    },

    getStoreAngel: function(a, b)
    {
        ay = Map.stores[a].ry - Map.stores[a].ly; 
        ax = Map.stores[a].rx - Map.stores[a].lx; 
        
        acy = (Map.stores[a].ry + Map.stores[a].ly) / 2; 
        acx = (Map.stores[a].rx + Map.stores[a].lx) / 2;

        bcy = (Map.stores[b].ry + Map.stores[b].ly) / 2; 
        bcx = (Map.stores[b].rx + Map.stores[b].lx) / 2;


        by = bcy - acy; 
        bx = bcx - acx; 

        ans = Math.acos( (ax*bx+ay*by) / (Map.getSqr(ax,ay)*Map.getSqr(bx,by)) );
        return ans; 
    },

    getAngel: function(a1x, a1y, a2x, a2y, b1x, b1y, b2x, b2y){
        ay = a2y - a1y; 
        ax = a2x - a1x; 
        by = b2y - b1y; 
        bx = b2x - b1x; 
        ans = Math.acos( (ax*bx+ay*by) / (Map.getSqr(ax,ay)*Map.getSqr(bx,by)) );
        return ans; 
    },

    drawTextLayer: function() {
        Map.txt = Array();
        Map.txtRect = Array();
        
        var padding = 10;
        
        for (i in storeNames) {
            Map.txt[i] = new Kinetic.Text({
                x: mapCordData[i].tx,
                y: mapCordData[i].ty,
                text: storeNames[i],
                fontSize: 31,
                fontFamily: 'Consolas, monospace',
                fill: '#444444'
            });

            Map.txtRect[i] = new Kinetic.Rect({
                x: mapCordData[i].tx - padding,
                y: mapCordData[i].ty - padding,
                width: Map.txt[i].getWidth() + padding * 2,
                height: 50,
                fill: 'white',
                stroke: 'white',
                opacity: 0.7,
                cornerRadius: 5
            }); 

            // An experiment
            Map.entrances[i] = new Kinetic.Polygon({
                points: [mapCordData[i].lx, mapCordData[i].ly, mapCordData[i].rx, mapCordData[i].ry, mapCordData[i].rx, mapCordData[i].ry+20, mapCordData[i].lx, mapCordData[i].ly + 20], 
                fill: '#fff',
                x:  Map.Settings.absX,
                y:  Map.Settings.absY,
                opacity: 0.5,
                stroke: '#aaa',
                strokeWidth: 0.5
            }); 

            (function(i) {
               Map.txt[i].on('mouseenter', function() {
                    Map.drawAbsLayer(i); 
                });
               Map.txtRect[i].on('mouseenter', function() {
                    Map.drawAbsLayer(i); 
                });
            })(i);

            Map.textLayer.add(Map.txtRect[i]); 
            Map.textLayer.add(Map.txt[i]); 
            Map.textLayer.drawScene(); 
            //Map.textLayer.add(Map.entrances[i]); 
        }
    },

    drawButtonLayer: function() {
        //console.log('drawButtonLayer'); 
        // console.log(Map.stage.getX(), Map.stage.getY()); 
        // buttons: zoom in and zoom out
        Map.buttonLayer.clear(); 
        Map.fullLegendLayer.clear(); 
        Map.rightLegendLayer.clear(); 

        var offX = 0, offY = 0; 

        var mapBtnZoomIn = new Kinetic.Circle({
            x: 50 - offX, y: 50 - offY, 
            radius: 40,
            fill: '#F2F3F2'
        });

        var mapBtnZoomInL = new Kinetic.Blob({
            points: [{
                x: 25 - offX, y: 50 - offY}, {
                x: 75 - offX, y: 50 - offY
            }],
            stroke: '#4E4F4E',
            strokeWidth: 10
        });

        var mapBtnZoomInR = new Kinetic.Blob({
            points: [{
                y: 25 - offY, x: 50 - offX }, {
                y: 75 - offY, x: 50 - offX   
            }],
            //scale: 1/Map.Settings.globalScale,
            stroke: '#4E4F4E',
            strokeWidth: 10
        });

        var mapBtnZoomOut = new Kinetic.Circle({
            x: 150 - offX,
            y: 50 - offY,
            radius: 40,
            fill: '#F2F3F2'
        });

        var mapBtnZoomOutL = new Kinetic.Blob({
            points: [{
                x: 125 - offX, y: 50 - offY }, {
                x: 175 - offX, y: 50 - offY
            }],
            stroke: '#4E4F4E',
            strokeWidth: 10
        });

        var store = new Kinetic.Rect({
            x: 20,
            y: 120,
            width: 170,
            height: 10,
            fill: '#4E4F4E',
        });

        var cp = new Kinetic.Rect({
            x: 10 - offX,
            y: 100 - offX,
            width: 180,
            height: 130,
            fill: '#f1f1f1',
        }); 

        var inflow = new Kinetic.Polygon({
            points: [40, 90, 60, 90, 60, 50, 80, 50, 50, 10, 20, 50, 40, 50],
            y: 120,
            fill: '#4E4F4E',
            stroke: '#4E4F4E',
            strokeWidth: 5
        });

        var outflow = new Kinetic.Polygon({
            points: [50, 90, 80, 50, 60, 50, 60, 10, 40, 10, 40, 50, 20, 50],
            x: 100,
            y: 120,
            fill: '#4E4F4E',
            stroke: '#4E4F4E',
            strokeWidth: 5
        }); 

        inflow.on('mousedown', function() { 
            if (Map.Settings.inflow){
                this.setAttr('fill', '#4E4F4E');
                Map.Settings.inflow = 0; 
            } else {
                this.setAttr('fill', '#f1f1f1');
                Map.Settings.inflow = 1; 
            }
        });
        outflow.on('mousedown', function() { 
            if (Map.Settings.outflow){
                this.setAttr('fill', '#4E4F4E');
                Map.Settings.outflow = 0; 
            } else {
                this.setAttr('fill', '#f1f1f1');
                Map.Settings.outflow = 1; 
            }
        });

        Map.flowLegend = new Image();
        Map.flowLegend.onload = function() {
            var yoda = new Kinetic.Image({
              x: Map.Settings.maxWidth + 200,
              y: 50,
              image: Map.flowLegend,
              width: 201,
              height: 201
            });
            Map.fullLegendLayer.add(yoda); 
            Map.fullLegendLayer.drawScene(); 
        };
        Map.flowLegend.src = 'img/FlowALL.PNG';
        
        Map.flowLegend2 = new Image();
        Map.flowLegend2.onload = function() {
            var yoda2 = new Kinetic.Image({
              x: Map.Settings.maxWidth + 200,
              y: 50,
              image: Map.flowLegend2,
              width: 201,
              height: 201
            });
            Map.rightLegendLayer.add(yoda2); 
            Map.rightLegendLayer.drawScene(); 
        };
        Map.flowLegend2.src = 'img/FlowOUT.PNG';
        

        mapBtnZoomIn.on('mousedown', function() { Map.zoomIn(); });
        mapBtnZoomInL.on('mousedown', function() { Map.zoomIn(); });
        mapBtnZoomInR.on('mousedown', function() { Map.zoomIn(); });

        mapBtnZoomOut.on('mousedown', function() { Map.zoomOut(); });
        mapBtnZoomOutL.on('mousedown', function() { Map.zoomOut(); });

        Map.buttonLayer.add(mapBtnZoomIn);
        Map.buttonLayer.add(mapBtnZoomInL);
        Map.buttonLayer.add(mapBtnZoomInR);
        /*
        Map.buttonLayer.add(cp); Map.buttonLayer.add(store);
        Map.buttonLayer.add(inflow); 
        Map.buttonLayer.add(outflow); 
        */
        Map.buttonLayer.add(mapBtnZoomOut);
        Map.buttonLayer.add(mapBtnZoomOutL);
        Map.buttonLayer.draw(); 
        Map.fullLegendLayer.draw(); 
        Map.rightLegendLayer.setOpacity(0); 
        Map.rightLegendLayer.draw(); 

    },

    _getZoomOffset:function(microZoom){
        var zoom = Map.Settings.globalScale * Map.Settings.zoom; 
        var res = {x: 0, y: 0}; 
        var oldY = Map.Settings.maxHeight/2 * zoom; 
        var oldX = Map.Settings.maxWidth/2 * zoom; 
        Map.Settings.zoom = Map.Settings.zoom + microZoom;
        zoom = Map.Settings.globalScale * Map.Settings.zoom; 
        res.y = Map.Settings.maxHeight/2 * zoom - oldY; 
        res.x = Map.Settings.maxWidth/2 * zoom - oldX;
        return res; 
    },

    _zoomIn:function(threshold){
        if (Map.Settings.zoom < threshold) {
            var offset = Map._getZoomOffset(Map.Settings.microZoomIn); 
            Map.updateZoomLevel(offset.y, offset.x);
        } else {
            Map.zoomAnimition.stop(); 
        }
    },

    zoomIn:function() {
        var toZoom = Map.Settings.zoom + Map.Settings.stepZoomIn; 
        //console.log("zoom in", toZoom);
        if (toZoom > Map.Settings.maxScale) return; 

        Map.zoomAnimition = new Kinetic.Animation(function(frame) {
            Map._zoomIn(toZoom); 
        }, toZoom);
        Map.zoomAnimition.start(); 

    },

    _zoomOut:function(threshold){
        if (Map.Settings.zoom > threshold) {
            var offset = Map._getZoomOffset(-Map.Settings.microZoomOut); 
            Map.updateZoomLevel(offset.y, offset.x);
        } else {
            Map.zoomAnimition.stop(); 
        }
    },

    zoomOut: function() {
        var toZoom = Map.Settings.zoom - Map.Settings.stepZoomOut; 
        if (toZoom < Map.Settings.minScale) return; 
        Map.zoomAnimition = new Kinetic.Animation(function(frame) {
            Map._zoomOut(toZoom); 
        }, toZoom);
        Map.zoomAnimition.start(); 
        //Map.updateFootTraffic();
    },

    updateZoomLevel:function (offY, offX) {
        var zoom = Map.Settings.globalScale * Map.Settings.zoom;
        Map.stage.setScale(zoom);
        Map.stage.setX(Map.stage.getX()-offX);
        Map.stage.setY(Map.stage.getY()-offY); 

        Map.buttonLayer.setScale(1/Map.Settings.zoom); 
        var zoom = Map.Settings.globalScale * Map.Settings.zoom; 

        var realH = Map.Settings.maxHeight * zoom;
        var realW = Map.Settings.maxWidth * zoom;
        Map.buttonLayer.setX(-Map.stage.getX()/(Map.Settings.globalScale*Map.Settings.zoom)); 
        Map.buttonLayer.setY(-Map.stage.getY()/(Map.Settings.globalScale*Map.Settings.zoom)); 
        Map.stage.drawScene(); 
    },

    updateFootTraffic: function (showFootTraffic) {
        //console.log('show foot traffic:', showFootTraffic);
        Map.Settings.footTrafficData = showFootTraffic;
        if (showFootTraffic) {
            Map.absLayer.setOpacity(1); 
            Map.absLayer.drawScene(); 
            //Map.footLayer.setOpacity(1);
            //Map.footLayer.drawScene(); 

        } else {
            Map.absLayer.setOpacity(0); 
            Map.absLayer.drawScene(); 
            //Map.footLayer.setOpacity(0);
            //Map.footLayer.drawScene();  
        }
    },

    queryCustomerData: function() {
        //console.log('Map.queryCustomerData');
        Map.State.value = 'customers';
        Map.queryStoreData(); 
    },

    queryProfitData: function() {
        console.log('Map.queryProfitData');
        Map.State.value = 'profit';
        Map.queryStoreData(); 
    },

    queryHour: function() {
        console.log('Map.queryHour');
        Map.State.timeType = 'hour'; 
        Map.queryStoreData(); 
    },

    queryDay: function() {
        console.log('Map.queryDay');
        Map.State.timeType = 'day'; 
        Map.queryStoreData(); 
    },

    queryMonth: function() {
        console.log('Map.queryMonth');
        Map.State.timeType = 'month'; 
        Map.queryStoreData(); 
    },


    drawBackgroundImg: function() {
        /*
         Map.backgroundLayer = new Kinetic.Layer({
         scale: Map.Settings.globalScale
         });
         var imgObj = new Image();
         imgObj.src = 'img/mall.png';

         var img = new Kinetic.Image({
         image: imgObj,
         alpha: 0.2
         });
         Map.backgroundLayer.add(img);
         Map.stage.add(Map.backgroundLayer); 
         */
    },

    drawstores: function() {
        console.log("Draw");
        Map.mapLayer.clear();
        Map.topLayer.clear();

        for (t in Map.stores) {
            var path = Map.stores[t].path;
            var group = new Kinetic.Group();

            //We have to set up a group for proper mouseover on stores
            group.add(path);
            Map.mapLayer.add(group);

            (function(path, t, group) {
                group.on('mouseover', function() {
                    //console.log("mouseover");
                    
                    path.setFill('#FFF');
                    path.setOpacity(0.3);
                    group.moveTo(Map.topLayer);
                    Map.topLayer.drawScene();

                });

                group.on('mouseenter', function() {
                    Map.drawAbsLayer(Map.stores[t].id); 
                    Map.Settings.flowall = 0; 
                    Map.drawButtonLayer(); 
                    
                    Map.fullLegendLayer.setOpacity(0); 
                    Map.rightLegendLayer.setOpacity(1);
                    Map.fullLegendLayer.drawScene();
                    Map.rightLegendLayer.drawScene(); 
                    

                }); 

                group.on('mouseout', function() {
                    //console.log("mouseout", t, Map.stores[t].color, Map.Settings.colors[Map.stores[t].color]);
                    
                    path.setFill(Map.Settings.colors[Map.stores[t].color]);
                    path.setOpacity(1);
                    group.moveTo(Map.mapLayer);
                    Map.mapLayer.draw();
                    Map.topLayer.drawScene();

                    Map.drawAbsLayer(-1); 
                    Map.Settings.flowall = 1; 

                    Map.rightLegendLayer.setOpacity(0);
                    Map.fullLegendLayer.setOpacity(1); 
                    
                    Map.fullLegendLayer.drawScene();
                    Map.rightLegendLayer.drawScene(); 
                    //Map.buttonLayer.drawScene(); 

                });

                group.on('click', function() {
                    
                    console.log("click on the store"); 
                    path.setOpacity(1);
                    path.setFill(Map.Settings.colors[Map.stores[t].color]);
                    //console.log(Map.stage.getMousePosition());
                    //console.log(path.attrs.id);
                    location.hash = path.attrs.id;
                    group.moveTo(Map.topLayer);

                    Map.topLayer.draw();
                    
                });
            })(path, t, group);
        }
        Map.mapLayer.draw();
    }
};

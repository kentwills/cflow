var details = (function(details, jQuery) {

////// visible /////////////////////////////////////////////////////////////////
    var visible = {};

    var margin = { top: 50, right: 10, bottom: 50, left: 150 },
        width = document.getElementById('detail').offsetWidth - margin.left - margin.right,
        height = document.getElementById('detail').offsetHeight - margin.top - margin.bottom,
        colors = ["#d6b963","#dadada","#8AA197", "#4b6b6e"],
        buckets = colors.length;

    visible.init = function() {
        var dataRequest = $.getJSON(
            //details.constructDataRquest2('stores','all','customers','hour',9,10,1,7,1,12)
            visible.constructDataRquest2('stores','all','profit','hour',9,22,1,7,1,12), function(response){
            var data = response;
            visible.response=data;
            renderHeatmap(data,'profit');
        }).error(function() { alert("error"); });
        return dataRequest;
    };

    visible.update = function(data, options) {
        d3.select("svg").remove();
        // Deep copy
        data=data;
        var newData = jQuery.extend(true, {}, data);
        renderHeatmap(filteredItems(newData), options);
        //renderHeatmap(binValues(newData), options);
    };

    visible.render = function(options) {

    };

    visible.constructDataRquest = function constructDataRequest(type,name,value,timeType, time1,time2){
        var s =  'http://nameless-bastion-3282.herokuapp.com/getData?'
            +'type='+type
            +'&name='+name
            +'&value='+value
            +'&timeType='+timeType
            +'&time1='+time1
            +'&time2='+time2;
        return s;
    };

    visible.constructDataRquest2 = function constructDataRequest(type,name,value,timeType, hour1,hour2,day1,day2,month1,month2){
        var s =  'http://nameless-bastion-3282.herokuapp.com/getData?'
            +'type='+type
            +'&name='+name
            +'&value='+value
            +'&timeType='+timeType
            +'&hour1='+hour1
            +'&hour2='+hour2
            +'&day1='+day1
            +'&day2='+day2
            +'&month1='+month1
            +'&month2='+month2
            ;
        return s;
    };

    visible.downloadDataUpdateHeatmap = function downloadDataUpdateHeatmap(file,options){
        $.getJSON(file, function(response){
            visible.update(response,options);
            visible.response=response;
        }).error(function() { alert("error"); });
    };

    visible.returnFilterItemList = function generateFilterItems(){
        $.each(visible.response.rows, function(i, data) {
            $("#stores ul")
            .append("<li><label>" + data.store + " :</label>\n"
                + "<input id='filter_" + data.store
                + "' type='checkbox' "
                + "checked='checked' "
                + "class='fa fa-check-square' /></li>");
        });
    };

    visible.response;

////// hidden //////////////////////////////////////////////////////////////////
    var OPAQUE = 1;
    var DEFAULT_OPACITY = OPAQUE;
    var WHITEOUT_OPACITY = DEFAULT_OPACITY * 0.05;
    
    function filteredItems(data){

        var length = $('#stores').find(':checkbox').length
        var checkbox = []
        $('#stores').find(':checkbox').each(function(i,d){
            if($('input#'+ d.id).is(':checked'))
                checkbox.push(1)
            else
                checkbox.push(0)
        });

        var i,j;

        var rows=data.rows;
        var newRows=[];
        for(i=0;i<data.rows.length;i++){
            if(checkbox[i]!=0){
                newRows.push(rows[i])
            }
        }
        data.rows=newRows;

        var newValues=[],count=0;
        for(i=0;i<data.values.length;i++){
                if(checkbox[data.values[i].store-1]!=0){
                    for(j=0;j<data.values[i].store;j++){
                        if(checkbox[j]==0)
                            count=count+1;
                    }
                    //console.log(count)
                    data.values[i].store=data.values[i].store-count;
                    newValues.push(data.values[i]);
                    count=0;
                }
        }
        data.values=newValues;

        return data;
    }

    function renderHeatmap(data,options){

        var svg = d3.select("#detail").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .data(data.values)
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var gridSize = Math.min(Math.floor(width / data.columns.length),Math.floor(height / visible.response.rows.length));

        svg.data(data.values);

        var min = d3.min(data.values, function (d) { return d.value; });
        var max = d3.max(data.values, function (d) { return d.value; });

        var colorScale = d3.scale.quantile()
            .domain([min,  max])
            .range(colors);

        var dayLabels = renderYAxis(svg,data,gridSize);

        var timeLabels = renderXAxis(svg,data,gridSize);

        var heatMap = renderCellsHeatmap(svg,data,gridSize);

        heatMap.transition().duration(100)
            .style("fill", function(d) { return colorScale(d.value); });

        heatMap.append("title").text(function(d) { return d.value; });

        var legend = renderLegend(svg, colorScale, data, gridSize, 10,options);
    }
    
    function renderXAxis(svg, data, gridSize){
        var timeLabels = svg.selectAll(".timeLabel")
            .data(data.columns)
            .enter().append("text")
            .text(function(d) { return d.time; })
            .attr("transform", function(d, i) {
                return "translate(" + gridSize  + ", -10) "
                + "translate(" + i * gridSize * 2 + ")";
            })
            .attr("y", 0)
            .attr("class",  "time")
            .on("mouseover", function(d, i) {
                svg.selectAll(".hour")
                    .filter(function(s) { return s.time != i + 1; })
                    .transition()
                    .style("opacity", WHITEOUT_OPACITY);
            })
            .on("mouseout", fade(DEFAULT_OPACITY, svg));
        return timeLabels;
    };

    function renderYAxis(svg,data,gridSize){
        var dayLabels = svg.selectAll(".dayLabel")
            .data(data.rows)
            .enter().append("text")
            .text(function (d) { return d.store; })
            .attr("x", 0)
            .attr("y", function (d, i) { return i * gridSize + 1; })
            .attr("transform", "translate(-6," + gridSize / 1.5 + ")")
            .attr("class", "store")
            .on("mouseover", function(d, i){
                svg.selectAll(".hour")
                    .filter(function(s) { return s.store!= i + 1; })
                    .transition()
                    .style("opacity", WHITEOUT_OPACITY);
                Map.drawAbsLayer(i+1);
            })
            .on("mouseout",fade(DEFAULT_OPACITY, svg));

        return dayLabels;
    };

    function coordinationMap(svg){
        fade(DEFAULT_OPACITY, svg);
        //Map.drawAbsLayer(-1);
    }

    function renderCellsHeatmap(svg, data, gridSize){
        var heatMap = svg.selectAll(".hour")
            .data(data.values)
            .enter().append("rect")
            .each(function (d, i) {
                d.x = i % data.columns.length;
                d.y = Math.floor(i/data.columns.length);
            })
            .attr("x", function(d) { return (d.time - 1) * gridSize*2; })
            .attr("y", function(d) { return (d.store - 1) * gridSize; })
            .attr("row_element", function(d, i ) { return d.store; })
            .attr("column_element", function(d, i ) { return d.time; })
            .attr("class", "hour")
            .attr("width", gridSize * 2)
            .attr("height", gridSize)
            .style("fill", colors[0])
            .on("mouseover", function(d) {
                svg.selectAll(".hour").transition().style("opacity", WHITEOUT_OPACITY);
                d3.select(this).transition().style("opacity", DEFAULT_OPACITY);
            })
            .on("mouseout", fade(DEFAULT_OPACITY, svg));
        return heatMap;
    };

    function renderLegend(svg, colorScale, data, gridSize, gridMargin,options) {
        var legendElementWidth = gridSize * 2;
        var legend = svg.selectAll(".legend")
            .data([0].concat(colorScale.quantiles()), function(d) { return d; })
            .enter().append("g")
            .attr("class", "legend");
        
        legend.append("rect")
            .attr("x", 2 * gridSize * data.columns.length + gridMargin)
            .attr("y", function(d, i) { return gridSize * i; })
            .attr("rx", 1)
            .attr("ry", 1)
            .attr("width", legendElementWidth)
            .attr("height", gridSize / 2)
            .style("fill", function(d, i) { return colors[i]; })
            .on("mouseover", function(d, i){
                svg.selectAll(".hour")
                    .filter(function(s) { return colorScale(s.value) !== colors[i]; })
                    .transition()
                    .style("opacity", WHITEOUT_OPACITY);
            })
            .on("mouseout", fade(DEFAULT_OPACITY, svg));


        legend.append("text")
            .text(function(d) {
                if(options==='profit')
                    return "> $" + Math.round(d);
                else
                    if(Math.round(d)<10)
                        return "> " + Math.round(d)+"  shoppers";
                    else
                        return "> " + Math.round(d)+" shoppers";
            })
            .attr("x", 2 * gridSize * data.columns.length
                    + legendElementWidth
                    + 2 * gridMargin)
            .attr("y", function(d, i) { return gridMargin + gridSize * i; })
            .attr("class", "legend");

        return legend;
    };
    
    function fade(opacity,svg) {
        return function(g, i) {
            Map.drawAbsLayer(-1);
            svg.selectAll(".hour")
                .transition()
                .style("opacity", opacity);
        };
    };

    function binValues(data){

        var rowsize = data.rows.length;
        var colsize = data.columns.length;
        var values=new Array(rowsize),i;
        for(i=0;i<rowsize;i++){
            values[i]=[]//new Array(data.columns.length);
            for(j=0;j<colsize;j++){
                values[i].push(0);
            }
        }

        i=0;
        data.values.forEach(function(data){//i=0;i<data.values.length;i++){
            var x = data.store-1;
            var y = data.time-1;
            values[x][y]=values[x][y]+data.value;
            //console.log(values[x][y]);
            //console.log(data.value)
        });

        data.values=[]

        for(i=0;i<rowsize;i++){
            for(j=0;j<colsize;j++){
                data.values.push({store:i+1,value:values[i][j],time:j+1})
            }
        }

        return data;
    }

// expose interface ////////////////////////////////////////////////////////
return visible;
})(window.details = window.details || {}, $, undefined);




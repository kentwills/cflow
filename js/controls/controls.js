/*
 * NOTE: This is mostly just fleshed out as an example. It's not fully
 *       functional. It won't look the same when it is.
 */
var controls = (function(controls, jQuery) {
////// visible /////////////////////////////////////////////////////////////////
    var visible = {};
    
    visible.showFootTraffic = true;
    visible.showProfitData = false;
    
    visible.init = function() {
        details.returnFilterItemList();
        registerHandlers();
        generateSliders();
    };
    
    
////// hidden //////////////////////////////////////////////////////////////////
    // pseudo-constants ////////////////////////////////////////////////////////
    
    // time-related
    var RANGE_STEP = 1;
    
    var TIME_RANGE_START = 9;
    var TIME_RANGE_END = 22;
    var TIME_RANGE_DEFAULT_VALUES = [TIME_RANGE_START, TIME_RANGE_END];
    var TIME_RANGE_STEP = 1;
    
    var DAY_RANGE_START = RANGE_STEP;
    var DAY_RANGE_END = 7;
    var DAY_RANGE_STEP = RANGE_STEP;
    var DAY_RANGE_DEFAULT_VALUES = [DAY_RANGE_START, DAY_RANGE_END];
    
    var MONTH_RANGE_START = 1;
    var MONTH_RANGE_END = 12;
    var MONTH_RANGE_STEP = RANGE_STEP;
    var MONTH_RANGE_DEFAULT_VALUES = [MONTH_RANGE_START, MONTH_RANGE_END];
    
    
    // functions ///////////////////////////////////////////////////////////////
    function registerHandlers() {
        $("#controls input:checkbox").change(function(event) {
            $(this).toggleClass("fa-check-square");
            $(this).toggleClass("fa-square-o");

            // filter checkbox events
            if (event.target.id.split('_')[0]=="filter") {
                $(this).trigger({
                    type: "filter",
                    checked: event.target.checked,
                    name: event.target.id.split('_')[1]
                });
            } else if(event.target.id=="foot-traffic") {
                 $(this).trigger({
                    type: "show_foot-traffic",
                    checked: event.target.checked,
                    name: event.target.id
                });
            }
        });

        $("#controls input:radio").change(function(event) {
            switch(event.target.id){
                case "select_profit-data":
                    if(event.target.className=="fa fa-circle-o"){
                        toggleOn("input#select_profit-data");
                        toggleOff("input#select_customer-data");
                    }
                break;
                case "select_customer-data":
                    if(event.target.className=="fa fa-circle-o"){
                        toggleOn("input#select_customer-data");
                        toggleOff("input#select_profit-data");
                    }
                break;
                case "time_hour":
                    if(event.target.className=="fa fa-circle-o"){
                        toggleOn("input#time_hour");
                        if($("input#time_day").hasClass("fa-dot-circle-o"))
                            toggleOff("input#time_day");
                        if($("input#time_month").hasClass("fa-dot-circle-o"))
                            toggleOff("input#time_month");
                    }
                    break;
                case "time_day":
                    if(event.target.className=="fa fa-circle-o"){
                        toggleOn("input#time_day");
                        if($("input#time_hour").hasClass("fa-dot-circle-o"))
                            toggleOff("input#time_hour");
                        if($("input#time_month").hasClass("fa-dot-circle-o"))
                            toggleOff("input#time_month");
                    }
                    break;
                case "time_month":
                    if(event.target.className=="fa fa-circle-o"){
                        toggleOn("input#time_month");
                        if($("input#time_day").hasClass("fa-dot-circle-o"))
                            toggleOff("input#time_day");
                        if($("input#time_hour").hasClass("fa-dot-circle-o"))
                            toggleOff("input#time_hour");
                    }
                    break;
                default:
                    break;
            }

            // hand off to coordinator
            $(this).trigger({
                type: "dataRadio",
                checked: event.target.checked,
                name: event.target.id.split('_')[1]
            });
        });
    }

    function toggleOff(control){
        $(control).toggleClass("fa-dot-circle-o");
        $(control).toggleClass("fa-circle-o");
    }

    function toggleOn(control){
        $(control).toggleClass("fa-circle-o");
        $(control).toggleClass("fa-dot-circle-o");
    }

    function generateSliders() {
        $("#time .slider").slider({
            range: true,
            min: TIME_RANGE_START,
            max: TIME_RANGE_END,
            values: TIME_RANGE_DEFAULT_VALUES,
            step: TIME_RANGE_STEP,
            create: function(event, ui) {
                updateTimeSlider(TIME_RANGE_DEFAULT_VALUES);
            },
            slide: function(event, ui) {
                updateTimeSlider(ui.values);
            },
            stop: function(event, ui) {
                updateTimeSlider(ui.values, true /* trigger coordinator-relevant event */);
            }
        });
        
        $("#day .slider").slider({
            range: true,
            min: DAY_RANGE_START,
            max: DAY_RANGE_END,
            values: DAY_RANGE_DEFAULT_VALUES,
            step: DAY_RANGE_STEP,
            create: function(event, ui) {
                updateDaySlider(DAY_RANGE_DEFAULT_VALUES);
            },
            slide: function(event, ui) {
                updateDaySlider(ui.values);
            },
            stop: function(event, ui) {
                updateDaySlider(ui.values, true /* trigger coordinator-relevant event */);
            }
        });
        
        $("#month .slider").slider({
            range: true,
            min: MONTH_RANGE_START,
            max: MONTH_RANGE_END,
            values: MONTH_RANGE_DEFAULT_VALUES,
            step: MONTH_RANGE_STEP,
            create: function(event, ui) {
                updateMonthSlider(MONTH_RANGE_DEFAULT_VALUES);
            },
            slide: function(event, ui) {
                updateMonthSlider(ui.values);
            },
            stop: function(event, ui) {
            updateMonthSlider(ui.values, true /* trigger coordinator-relevant event */);
            }
        });
    }
    
    function getTimeRange() { return $("#time .slider").slider("values"); }
    
    function getDayRange() { return $("#day .slider").slider("values"); }
    
    function getMonthRange() { return $("#month .slider").slider("values"); }
    
    function updateTimeSlider(values, propagate) {
        var start = timeToLabel(values[0]);
        var end = timeToLabel(values[1]);
        updateSlider("time", start, end, propagate);
    }
    
    function updateDaySlider(values, propagate) {
        var start = " " + dayToLabel(values[0]);
        var end = dayToLabel(values[1]) + " ";
        updateSlider("day", start, end, propagate);
    }
    
    function updateMonthSlider(values, propagate) {
        var start = " " + monthToLabel(values[0]);
        var end = monthToLabel(values[1]) + " ";
        updateSlider("month", start, end, propagate);
    }
    
    function updateSlider(name, start, end, propagate) {
        $("#" + name + " .start").text(start);
        $("#" + name + " .end").text(end);
        
        // only attempt a network hit if prompted to
        if (propagate === true) {
            var timeRange = getTimeRange();
            var dayRange = getDayRange();
            var monthRange = getMonthRange();
            
            var event = {
                type: "timeFiltered",
                time: {start: timeRange[0], end: timeRange[1]},
                day: {start: dayRange[0], end: dayRange[1]},
                month: {start: monthRange[0], end: monthRange[1]}
            };
            
            // ...and only incur a network hit if the filters have actually changed
            if (this.lastTriggered === undefined
                || event.time.start !== this.lastTriggered.time.start
                || event.time.end !== this.lastTriggered.time.end
                || event.day.start !== this.lastTriggered.day.start
                || event.day.end !== this.lastTriggered.day.end
                || event.month.start !== this.lastTriggered.month.start
                || event.month.end !== this.lastTriggered.month.end) {
                this.lastTriggered = event;
                $(this).trigger(event);
            }
        }
    }
    
    function timeToLabel(time) {
        var units = time > 11 ? "pm" : "am";
        var time = time !== 12 ? time % 12 : time;
        return (time < 10 ? " " + time : time) + units;
    }
    
    function dayToLabel(day) {
        return {
            1: "Mon",
            2: "Tue",
            3: "Wed",
            4: "Thu",
            5: "Fri",
            6: "Sat",
            7: "Sun"
        }[day];
    }
    
    function monthToLabel(month) {
        return {
            1: "Jan",
            2: "Feb",
            3: "Mar",
            4: "Apr",
            5: "May",
            6: "Jun",
            7: "Jul",
            8: "Aug",
            9: "Sep",
            10: "Oct",
            11: "Nov",
            12: "Dec"
        }[month];
    }
    
    // expose interface ////////////////////////////////////////////////////////
    return visible;
})(window.controls = window.controls || {}, $, undefined);

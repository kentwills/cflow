/*
 * NOTE: this is also incomplete. And just here as an example.
 */
var coordinator = (function(coordinator, jQuery){
    // visible /////////////////////////////////////////////////////////////////
    var visible = {};
    
    visible.init = function() {

        var request = details.init();
        //Must come after details because data needs to be loaded
        $.when(request).done(function (request){controls.init()});
        registerHandlers();
    };

    visible.timeStart = 9;
    visible.timeEnd = 22;
    visible.dayStart = 1;
    visible.dayEnd = 7;
    visible.monthStart=1;
    visible.monthEnd=12;
    
    // hidden //////////////////////////////////////////////////////////////////


    function registerHandlers() {
        $(this).on("timeFiltered", function(event) {
            visible.timeStart = event.time.start;
            visible.timeEnd = event.time.end;
            visible.dayStart = event.day.start;
            visible.dayEnd = event.day.end;
            visible.monthStart = event.month.start;
            visible.monthEnd = event.month.end;

            var time = getSliderValues();
            details.downloadDataUpdateHeatmap(details.constructDataRquest2('stores','all',getType(),getBin(),
                time.hour.start,time.hour.end,
                time.day.start,time.day.end,
                time.month.start,time.month.end),getType());
            Map.updateTime(visible.timeStart, visible.timeEnd, visible.dayStart, visible.dayEnd, visible.monthStart, visible.monthEnd);
            /*console.log("HANDLING TIME FILTERED EVENT");
            console.log("  time " + timeStart + " " + timeEnd);
            console.log("  day " + dayStart + " " + dayEnd);
            console.log("  month " + monthStart + " " + monthEnd);*/
        });
        
        //Heatmap Events
        $(this).on("store", function(event) {
            console.log(event);
            console.log(event.name);
        });
        $(this).on("time", function(event) {
            console.log(event);
            console.log(event.time);
        });
        $(this).on("value", function(event) {
            console.log(event);
            console.log(event.value);
        });
        $(this).on("legend", function(event) {
            console.log(event);
            console.log(event.legend);
        });
        //Control Panel Events
        $(this).on("show_foot-traffic", function(event) {
            Map.updateFootTraffic($('#foot-traffic').is(":checked"));
        });

        $(this).on("filter", function(event) {
            $('#filtertime_hour')
            reloadHeatmap();
        });
        //Control Panel Events
        $(this).on("dataRadio", function(event) {
            Map.updateType(getType());
            var time = getSliderValues();
            details.downloadDataUpdateHeatmap(details.constructDataRquest2('stores','all',getType(),getBin(),
                time.hour.start,time.hour.end,
                time.day.start,time.day.end,
                time.month.start,time.month.end)
                ,getType());

        });
        //re-draw for window resizing
        $(window).resize(function() {
            reloadHeatmap();
        });
    }

    function reloadHeatmap(){
        details.update(details.response);
        Map.reloadMap(); 
    }

    function getBin(){
        if($('input#time_hour').is(":checked"))
            return 'hour';
        else if($('input#time_day').is(":checked"))
            return 'day';
        else
            return 'month';
    }
    function getType(){
        if($('input#select_customer-data').is(":checked"))
            return 'customers';
        else
            return 'profit';
    }

    function getSliderValues(){
        hour = {start:visible.timeStart,end:visible.timeEnd};
        day = {start:visible.dayStart,end:visible.dayEnd};
        month ={start:visible.monthStart,end:visible.monthEnd};
        data = {hour:hour,day:day,month:month};
        return data;
    }

    // expose interface ////////////////////////////////////////////////////////
    return visible;
})(window.controller = window.controller || {}, $, undefined);
